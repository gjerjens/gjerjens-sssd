# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.1
- Fix Array handling issue
- Add LICENSE file
- Puppet strings generated REFERENCE.md

## Release 0.1.0
- Initial release

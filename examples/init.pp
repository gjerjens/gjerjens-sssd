node default {

  notify { 'enduser-before': }
  notify { 'enduser-after': }

  class { 'sssd':
    require => Notify['enduser-before'],
    before  => Notify['enduser-after'],
  }

}

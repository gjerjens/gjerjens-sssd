# @summary Base sssd class
#
# Installs and configures SSSD
#
# @example Declaring the class
#   include ::sssd
#
# @param config Hash containing entire SSSD config.
#
# @param config_file
#   Specifies a file for SSSD's configuration info. Default value: '/etc/sssd/sssd.conf'.
#
# @param config_file_ensure Whether the SSSD config file should be present or absent. Default value: present.
#
# @param config_file_mode
#   Specifies a file mode for the SSSD configuration file. Default value: '0600'.
#
# @param config_epp
#   Specifies an absolute or relative file path to an EPP template for the config file.
#   Example value: 'sssd/sssd.conf.epp'. A validation error is thrown if both this **and** the `config_template` parameter are specified.
#
# @param config_template
#   Specifies an absolute or relative file path to an ERB template for the config file.
#   Example value: 'sssd/sssd.conf.erb'. A validation error is thrown if both this **and** the `config_epp` parameter are specified.
#
# @param package_ensure
#   Whether to install the SSSD package, and what version to install. Values: 'present', 'latest', or a specific version number.
#   Default value: 'present'.
#
# @param package_manage
#   Whether to manage the SSSD package. Default value: true.
#
# @param package_name
#   Specifies the SSSD package to manage. Default value: ['sssd']
#
# @param service_enable
#   Whether to enable the SSSD service at boot. Default value: true.
#
# @param service_ensure
#   Whether the SSSD service should be running. Default value: 'running'.
#
# @param service_manage
#   Whether to manage the SSSD service.  Default value: true.
#
# @param service_name
#   The SSSD service to manage. Default value: varies by operating system.
#
# @param service_provider
#   Which service provider to use for SSSD. Default value: 'undef'.
#
# @param service_hasstatus
#   Whether service has a functional status command. Default value: true.
#
# @param service_hasrestart
#   Whether service has a restart command. Default value: true.
#
class sssd (
  Hash $config,
  Stdlib::Absolutepath $config_file,
  String $config_file_ensure,
  String $config_file_mode,
  Optional[String] $config_epp,
  Optional[String] $config_template,
  String $package_ensure,
  Boolean $package_manage,
  Array[String] $package_name,
  Boolean $service_enable,
  Enum['running', 'stopped'] $service_ensure,
  Boolean $service_manage,
  String $service_name,
  Optional[String] $service_provider,
  Boolean $service_hasstatus,
  Boolean $service_hasrestart,

) {

  contain sssd::install
  contain sssd::config
  contain sssd::service

  Class['::sssd::install']
  -> Class['::sssd::config']
  ~> Class['::sssd::service']

}

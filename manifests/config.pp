# @summary
#   This class handles the configuration file.
#
# @api private
#
class sssd::config {

  #If both epp and erb are defined, throw validation error.
  #Otherwise use the defined erb/epp template, or use default
  if $sssd::config_epp and $sssd::config_template {
    fail('Cannot supply both config_epp and config_template templates for sssd config file.')
  } elsif $sssd::config_template {
    $config_content = template($sssd::config_template)
  } elsif $sssd::config_epp {
    $config_content = epp($sssd::config_epp)
  } else {
    $config_content = epp('sssd/sssd.conf.epp')
  }

  file { $sssd::config_file:
    ensure  => $::sssd::config_file_ensure,
    owner   => 0,
    group   => 0,
    mode    => $::sssd::config_file_mode,
    content => $config_content,
  }

}

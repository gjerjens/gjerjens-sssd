# @summary
#   This class handles the SSSD service.
#
# @api private
#
class sssd::service {

  if $sssd::service_manage == true {
    service { 'sssd':
      ensure     => $sssd::service_ensure,
      enable     => $sssd::service_enable,
      name       => $sssd::service_name,
      provider   => $sssd::service_provider,
      hasstatus  => $sssd::service_hasstatus,
      hasrestart => $sssd::service_hasrestart,
    }
  }

}

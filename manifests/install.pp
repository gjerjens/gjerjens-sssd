# @summary
#   This class handles SSSD packages.
#
# @api private
#
class sssd::install {

  if $sssd::package_manage {

    package { $sssd::package_name:
      ensure => $sssd::package_ensure,
    }

  }

}

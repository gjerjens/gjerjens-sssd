# sssd

#### Table of Contents

1. [Overview](#overview)
2. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Credits](#credits)

## Description

This module installs and configures SSSD (System Security Services Daemon)

[SSSD][0] is used to provide access to identity and authentication remote resource through a common framework that can provide caching and offline support to the system.

## Usage

Example configuration:

```puppet
class {'::sssd':
  config => {
    'sssd' => {
      'domains'             => 'example.com',
      'config_file_version' => 2,
      'services'            => ['nss', 'pam'],
    },
    'domain/ad.example.com' => {
      'ad_domain'                      => 'example.com',
      'krb5_realm'                     => 'EXAMPLE.COM',
      'realmd_tags'                    => 'manages-system joined-with-adcli',
      'cache_credentials'              => true,
      'id_provider'                    => 'ad',
      'krb5_store_password_if_offline' => true,
      'default_shell'                  => '/bin/bash',
      'ldap_id_mapping'                => false,
      'use_fully_qualified_names'      => false,
      'fallback_homedir'               => '/home/%u@%d',
      'access_provider'                => 'ad',
    }
  }
}
```

...or the same config in Hiera:

```yaml
sssd::config:
  sssd:
    domains: 'example.com'
    config_file_version: 2
    services:
      - 'nss'
      - 'pam'
  'domain/example.com':
    ad_domain: 'example.com'
    krb5_realm: 'EXAMPLE.COM'
    realmd_tags: 'manages-system joined-with-adcli'
    cache_credentials: true
    id_provider: 'ad'
    krb5_store_password_if_offline: true
    default_shell: '/bin/bash'
    ldap_id_mapping: false
    use_fully_qualified_names: false
    fallback_homedir: '/home/%u@%d'
    access_provider: 'ad'
```

Will be represented in sssd.conf like this:

```ini
[sssd]
domains = example.com
config_file_version = 2
services = nss, pam

[domain/example.com]
ad_domain = example.com
krb5_realm = EXAMPLE.COM
realmd_tags = manages-system joined-with-adcli
cache_credentials = true
id_provider = ad
krb5_store_password_if_offline = true
default_shell = /bin/bash
ldap_id_mapping = false
use_fully_qualified_names = false
fallback_homedir = /home/%u@%d
access_provider = ad
```

## Limitations

This module only handles the SSSD package, config and service.
All other special requirements (such as oddjobd, authselect, adcli etc.) is out of scope.

### Tested on

* CentOS 8
* Fedora 32
* Ubuntu 18.04

## Credits

* Most of the puppet code is based on [puppetlabs-ntp][1] by Puppet

[0]: https://docs.pagure.org/SSSD.sssd/
[1]: https://github.com/puppetlabs/puppetlabs-ntp
